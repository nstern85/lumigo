import time
from multiprocessing import Value
from queue import Queue
from threading import Thread
from LambdaFunctionWrapper import LambdaFunctionWrapper

TIME_TO_LIVE = 15


class LambdaManager:
    def __init__(self, fw_queue: Queue):
        super().__init__()
        self._processes = []
        self._queue = Queue()
        self._fw_queue = fw_queue
        self._invocations = Value('d', 0)

    def start(self):
        t = Thread(target=self._consume)
        t.start()

    def _consume(self):
        while True:
            if not self._queue.empty():
                queue_item = self._queue.get()
                p = self._get_lambda_function_wrapper()
                p.enqueue(queue_item)
            else:
                time.sleep(0.5)
            self._remove_redundant_processes()

    def enqueue(self, message: str):
        self._queue.put(message)

    def get_stats(self):
        return {
            "active_instances": len(self._processes),
            "total_invocations": self._invocations.value
        }

    def _remove_redundant_processes(self):
        items_to_delete = []
        for process in self._processes:
            if process.living_time > TIME_TO_LIVE:
                items_to_delete.append(process)
        for to_del in items_to_delete:
            to_del.terminate()
            self._processes.remove(to_del)

    def _get_lambda_function_wrapper(self) -> LambdaFunctionWrapper:
        if not self._processes or all([p.is_active for p in self._processes]):
            lambda_process = LambdaFunctionWrapper(self._fw_queue, self._invocations)
            self._processes.append(lambda_process)
            return lambda_process
        else:
            for process in self._processes:
                if not process.is_active:
                    return process
