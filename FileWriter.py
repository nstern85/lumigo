from multiprocessing import Queue
from threading import Thread


class FileWriter:
    def __init__(self, fw_queue: Queue, path="output.txt"):
        super().__init__()
        self._queue = fw_queue
        self._path = path

    def start(self):
        t = Thread(target=self._consume)
        t.start()

    def _consume(self):
        while True:
            message = self._queue.get()
            with open(self._path, "a+") as fw:
                fw.write(message + "\n")
