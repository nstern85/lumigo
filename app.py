from multiprocessing import Queue
from flask import Flask, request, jsonify
from FileWriter import FileWriter
from LambdaManager import LambdaManager
from utils import get_logger

logger = get_logger()
app = Flask(__name__)
fw_queue = Queue()
file_writer = FileWriter(fw_queue)
file_writer.start()
lambda_manager = LambdaManager(fw_queue)
lambda_manager.start()


@app.errorhandler(Exception)
def handle_bad_request(ex):
    url = None
    try:
        url = request.url
    except Exception as ex:
        logger.error(f"internal error {ex}")
    return "Bad Request", 400


@app.route('/messages', methods=['POST'])
def write_messages():
    message = request.json.get("message")
    if message:
        lambda_manager.enqueue(message)
        return 'OK', 200
    else:
        logger.warning("missing message")
        return "Bad Request", 400


@app.route('/statistics', methods=['GET'])
def statistics():
    return jsonify(lambda_manager.get_stats())


if __name__ == '__main__':
    try:
        app.run(host="127.0.0.1", port=8000)
    except Exception as ex:
        logger.error("internal error", ex)
