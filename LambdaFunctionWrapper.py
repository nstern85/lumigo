import time
from multiprocessing import Process, Queue, Value, Lock


def func(q, fw_queue, invocations, start_time, end_time):
    while True:
        start_time.value = time.time()
        item = q.get()
        time.sleep(5)
        with Lock():
            invocations.value += 1
        fw_queue.put(item)
        end_time.value = time.time()


class LambdaFunctionWrapper:
    def __init__(self, fw_q: Queue, invocations: Value):
        self._queue = Queue()
        current_time = time.time()
        self._start_time = Value('d', current_time)
        self._end_time = Value('d', current_time)
        self._process = Process(target=func, args=(self._queue, fw_q, invocations, self._start_time, self._end_time))
        self._process.start()

    def is_active(self):
        return self._end_time.value < self._start_time.value

    @property
    def living_time(self):
        return time.time() - self._end_time.value

    def enqueue(self, message: str):
        self._queue.put(message)

    def terminate(self):
        try:
            self._process.terminate()
        except Exception as ex:
            print("could not terminate process", ex)
